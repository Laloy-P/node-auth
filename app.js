const express = require('express');
const mongoose = require('mongoose');
const authRoutes = require ('./routes/authRoutes');
const cookieParser = require('cookie-parser');
const { requireAuth, checkUser } = require('./middleware/authMiddleware');


const app = express();

require('dotenv').config();

// middleware
app.use(express.static('public'));
app.use(express.json());
app.use(cookieParser());

// view engine
app.set('view engine', 'ejs');

//Database credentials
const CLUSTER=process.env.CLUSTER;
const DB_NAME=process.env.DB_NAME;
const DB_USER=process.env.DB_USER;
const DB_PWD=process.env.DB_PWD;

// database connection
const dbURI = `mongodb+srv://${DB_USER}:${DB_PWD}@${CLUSTER}.wtnwt.mongodb.net/${DB_NAME}`;
mongoose.connect(dbURI, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex:true })
  .then((result) => app.listen(3000))
  .catch((err) => console.log(err));

// routes
app.get('*', checkUser);
app.get('/', (req, res) => res.render('home'));
app.get('/smoothies', requireAuth, (req, res) => res.render('smoothies'));
app.use(authRoutes);
