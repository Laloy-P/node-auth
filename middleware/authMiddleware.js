const jwt = require ('jsonwebtoken');
const User = require('../models/User');

const requireAuth = (req, res, next) => {
    const token  = req.cookies.jwt;
    
    // check if json web token exist & is verified.
    if (token) {
        jwt.verify(token, process.env.JWT_SECRET, (error, decodedToken) => {
            if (error){
                console.log(error.message);
                res.redirect('/login')
            } else {
                console.log(decodedToken);
                next();
            }
        });
    } else {
        res.redirect('/login');
    }
};

//check currentUser
const checkUser = (req, res, next) => {
    const token  = req.cookies.jwt;
    res.locals.currentUser = null;
    
    if (token) {
        jwt.verify(token, process.env.JWT_SECRET, async (error, decodedToken) => {
            if (error){
                console.log('error in chkuser',error.message);
                next();
            } else {
                console.log(decodedToken);
                let user = await User.findById(decodedToken.id);
                res.locals.currentUser = user;
                next();
            }
        });
    } else {
        next ();
    }
};

module.exports = { requireAuth, checkUser };