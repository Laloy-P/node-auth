const mongoose = require('mongoose');
const { isEmail } = require('validator');
const bcrypt = require ('bcrypt');
const {MailError, PasswordError} = require('../errors/lginFormErrors');

const userSchema = mongoose.Schema({
    email: {
        type: String,
        required: [true, 'Couriel requis.'],
        unique: true,
        lowercase: true,
        validate: [isEmail, 'Ce Couriel n\'est pas valide.']
    },
    password: {
        type: String,
        required: [true, 'Mot de passe requis.'],
        minlength: [6,'Le mot de passe doit faire 6 caractere minimum.']
    },
});

//declence une  function apres l'ajout d'un utilisateur en base. <== Mangoose hook
userSchema.post('save', (doc, next)=>{
    console.log('Nouvel utilisateur ajouter a la bdd.', doc);
   next();// comme pour les middleware on a besoin d'indiquer a la fonction de continuer l'execution du programme.
});

// declenche une fonction avant l'ajout d'in utilisateur.
// pas de arrow fonction pour que lqareferenceT his soit
// l'instance de l'objet user creer dans le controleur[authController l32]
userSchema.pre('save', async function (next){
    const salt = await bcrypt.genSalt();
    this.password = await bcrypt.hash(this.password, salt);
    next()
})

//static mthod to login user
userSchema.statics.login = async function(email, password) {
    const user = await this.findOne({email});
    if (user) {
        const auth = await bcrypt.compare(password, user.password);
        if (auth){
            return user;
        }
        throw new PasswordError('incorrect password');
    }
    throw new MailError('incorrect email')
}


const User = mongoose.model('user', userSchema);

module.exports = User;