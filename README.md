# Node Auth

## Implementation d'un systeme d'authentification

Utilisation des technos suivante : 

    - Node
    - Express
    - MongoDb (Atlas)
    - Jwt

## A propos

Prise en main de l'authentifiacation via JWT, et utilisation de Node et express.

## Lancer le projet

    1- Vous devez avoir configuré une bdd via [MongDB Atlas](https://www.mongodb.com/cloud/atlas)
    2- Remplacer les informations de connexion dans le fichier .env-sample et le renomer en .env
    3- run : npm install
    4- run : node app.js OU: nodemon app.js
