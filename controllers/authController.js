const User = require('../models/User');
const jwt = require ('jsonwebtoken');
const {MailError, PasswordError} = require('../errors/lginFormErrors');

//3 jours en seconde :
const maxAgeSecond = 3 * 24 * 60 * 60;
//3 jours en miliseconde :
const maxAgeMiliSecond = 3 * 24 * 60 * 60 * 1000;

//handleerrors
const handlerrors = (err) => {
    console.log(err.message, err.code);
    let errors = {email:'', password:''};

    //duplicate error code
    if (err.code === 11000) {
        errors.email = 'Ce courriel est deja enregistrer.';
        return errors;
    }

    if(err instanceof MailError){
        errors.email = 'Ce courriel n\'est pas enregistrer.';
    }

    if(err instanceof PasswordError){
        errors.password = 'Mot de passe incorrect.';
    }


    // validation errors
    if(err.message.includes('user validation failed:')){
        Object.values(err.errors).forEach(({properties}) => {
            errors[properties.path] = properties.message;
        })
    }

    return errors;
}

const createToken = (id) => {
    return jwt.sign({ id }, `${process.env.JWT_SECRET}`, {expiresIn: maxAgeSecond});
}

module.exports.signup_get = (req, res)=>{
    res.render('signup');
}

module.exports.signup_post = async (req, res)=>{
    const { email, password } = req.body;
    try {
        const user = await User.create({email, password});
        const token = createToken(user._id);
        res.cookie('jwt', token, {httpOnly:true, maxAge: maxAgeMiliSecond, secure:false});
        res.status(201).json({user: user._id});
    }
    catch (error) {
        const errors = handlerrors(error);
        res.status(400).json({errors});
    }
}

module.exports.login_get = (req, res)=>{
    res.render('login');
}

module.exports.login_post = async (req, res)=>{
    const {email, password } = req.body;

    try {
        const user = await User.login(email, password);
        const token = createToken(user._id);
        res.cookie('jwt', token, {httpOnly:true, maxAge: maxAgeMiliSecond, secure:false})
        res.status(200).json({user: user._id})
    } catch (error) {
        const errors = handlerrors(error);
        res.status(400).json({errors});
    }
}

module.exports.logout_get = (req, res) => {
    res.cookie('jwt', '', {maxAge: 1});
    res.redirect('/');
}