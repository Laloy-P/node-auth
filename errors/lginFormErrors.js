class MailError extends Error {

    constructor (...params){
        super(...params);
        if(Error.captureStackTrace ){
            Error.captureStackTrace(this, MailError);
        }
        this.name = 'MailValidationError';
        this.date = new Date();
    }
}

class PasswordError extends Error {

    constructor (...params){
        super(...params);
        if(Error.captureStackTrace) {
            Error.captureStackTrace(this, PasswordError);
        }
        this.name = 'PasswordValidationError';
        this.date = new Date();
    }
}

module.exports = { 
    MailError, 
    PasswordError
};